/*
  Jquery Validation using jqBootstrapValidation
   example is taken from jqBootstrapValidation docs
  */
$(function() {

    $("input,textarea").jqBootstrapValidation(
        {
            preventSubmit: true,
            submitError: function($form, event, errors) {
                // something to have when submit produces an error ?
                // Not decided if I need it yet
            },
            submitSuccess: function($form, event) {
                event.preventDefault(); // prevent default submit behaviour
                // get values from FORM
                var name = $("input#input_nombre").val();
                var email = $("input#input_correo").val();
                var number = $("input#input_telefono").val();
                var firstName = name; // For Success/Failure Message
                // Check for white space in name for Success/Fail message
                if (firstName.indexOf(' ') >= 0) {
                    firstName = name.split(' ').slice(0, -1).join(' ');
                }
                $.ajax({
                    url: "./bin/contact_me.php",
                    type: "POST",
                    data: {name: name, number: number, email: email},
                    cache: false,
                    success: function() {
                        // Success message
                        $('#success').html("" +
                            "<div class='btn info text-white mt-2 text-blue border-success w-75 mb-4'>" +
                                "<strong class='text-white'>Mensaje enviado. </strong>" +
                            "</div>" +
                            "");

                        //clear all fields
                        $('#contactForm').trigger("reset");
                    },
                    error: function() {
                        console.log(error)
                        // Fail message
                        $('#success').html("" +
                            "<div class='btn info text-white mt-2 text-blue border-danger w-75 mb-4'>" +
                                "<strong class='text-white'>Lo siento "+firstName+" el servidor no responde...</strong> <span class='text-white'>Puedes enviar un correo a la siguiente direccion <a href='mailto:contacto@cargame.mx?Subject=Contacto'>contacto@cargame.mx</a> ? Gracias</span>" +
                            "</div>" +
                            "");
                        //clear all fields
                        $('#contactForm').trigger("reset");
                    },
                })
            },
            filter: function() {
                return $(this).is(":visible");
            },
        });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});